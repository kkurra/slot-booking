from typing import Final
import openpyxl
import psycopg2

#subject data

def get_subject_data():
    list = []
    file = openpyxl.load_workbook("subject.xlsx")
    f = file.active
    col = f.max_column
    row = f.max_row
    cursor.execute('SELECT subject_id FROM subject order by subject_id desc')
    g_row=cursor.fetchone()
    if g_row != None:
        sub_id = int(g_row[0])+1
    else:
        sub_id = 1
    for j in range(sub_id, row + 1):
        l = []
        for i in range(1, col + 1):
            data = f.cell(row=j, column=i)
            temp = data.value
            l.append(temp)
        list.append(l)
    return list
try:
    conn = None
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        port="5432",
        password="123")
    cursor = conn.cursor()
    query = "INSERT INTO subject (subject_id,subject_name) VALUES (%s,%s)"
    table_data = get_subject_data()
    file = openpyxl.load_workbook("subject.xlsx")
    f = file.active
    col = f.max_column
    row = (f.max_row)
    cursor.execute('SELECT subject_id FROM subject order by subject_id desc')
    g_row = cursor.fetchone()
    if g_row != None:
        subject_id = int(g_row[0]) + 1
    else:
        subject_id = 1
    for k in range(0, len(table_data)):

        input1 = (subject_id ,table_data[k][1])
        cursor.execute(query, input1)
        subject_id+=1
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
    pass
finally:
    if conn is not None:
        pass


# batch data

def get_batch_data():
    list = []
    file = openpyxl.load_workbook("batch.xlsx")
    f = file.active
    col = f.max_column
    row = f.max_row
    cursor.execute('SELECT batch_id FROM batch order by batch_id desc')
    g_row=cursor.fetchone()
    if g_row != None: sub_id = int(g_row[0])+1
    else: sub_id = 1
    for j in range(sub_id, row + 1):
        l = []
        for i in range(1, col + 1):
            data = f.cell(row=j, column=i)
            temp = data.value
            l.append(temp)
        list.append(l)
    return list
try:
    conn = None
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        port="5432",
        password="123")
    cursor = conn.cursor()
    query = "INSERT INTO batch (batch_id,batch_name) VALUES (%s,%s)"
    table_data = get_batch_data()
    file = openpyxl.load_workbook("batch.xlsx")
    f = file.active
    col = f.max_column
    row = (f.max_row)
    cursor.execute('SELECT batch_id FROM batch order by batch_id desc')
    g_row = cursor.fetchone()
    if g_row != None:
        batch_id = int(g_row[0]) + 1
    else:
        sub_id = 1
    for k in range(0, len(table_data)):
        input =(batch_id, table_data[k][1])
        cursor.execute(query, input)
        batch_id+=1
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
    pass
finally:
    if conn is not None:
        pass


#Student data

def get_student_data():
    list = []
    file = openpyxl.load_workbook("student.xlsx")
    f = file.active
    col = f.max_column
    row = f.max_row
    cursor.execute('SELECT student_id FROM student order by student_id desc')
    g_row=cursor.fetchone()
    if g_row != None: s_id = int(g_row[0])+1
    else: s_id = 1
    for j in range(s_id, row+1):
        l = []
        for i in range(1, col + 1):
            data = f.cell(row=j, column=i)
            temp = data.value
            l.append(temp)
        list.append(l)
    return list
try:
    conn = None
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        port="5432",
        password="123")
    cursor = conn.cursor()
    query = "INSERT INTO student (student_id,student_name,mobile_no, email_id, batch_id) VALUES (%s,%s,%s,%s,%s)"
    table_data = get_student_data()
    file = openpyxl.load_workbook("student.xlsx")
    f = file.active
    col = f.max_column
    row = (f.max_row)
    cursor.execute('Select student_id FROM student order by student_id desc')
    
    g_row = cursor.fetchone()
    if g_row != None:
        student_id = int(g_row[0])+1
    else:
        student_id = 1

    for k in range(0, len(table_data)):
        batch_name = table_data[k][4]
        line = cursor.execute("Select batch_id FROM batch WHERE batch_name='%s'" %(batch_name))
        batch_id = cursor.fetchone()
        input = (student_id,table_data[k][1], table_data[k][2] ,table_data[k][3], batch_id)
        cursor.execute(query, input)
        student_id+=1
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
    pass
finally:
    if conn is not None:
        pass


#Teacher data

def get_teacher_data():
    list = []
    file = openpyxl.load_workbook("teacher.xlsx")
    f = file.active
    col = f.max_column
    row = f.max_row
    cursor.execute('SELECT teacher_id FROM teacher order by teacher_id desc')
    g_row = cursor.fetchone()
    if g_row != None:
        sub_id = int(g_row[0]) + 1
    else:
        sub_id = 1
    for j in range(sub_id, row + 1):
        l = []
        for i in range(1, col + 1):
            data = f.cell(row=j, column=i)
            temp = data.value
            l.append(temp)
        list.append(l)
    return list
try:
    conn = None
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        port="5432",
        password="123")
    cursor = conn.cursor()
    query = "INSERT INTO teacher (teacher_id,teacher_name,mobile_no, email_id,subject_id) VALUES (%s,%s, %s,%s,%s)"
    table_data = get_teacher_data()
    file = openpyxl.load_workbook("teacher.xlsx")
    f = file.active
    col = f.max_column
    row = (f.max_row)
    cursor.execute('Select teacher_id FROM teacher order by teacher_id desc')
    g_row = cursor.fetchone()
    if g_row != None:
        teacher_id = int(g_row[0]) + 1
    else:
        teacher_id = 1
    for k in range(0, len(table_data)):
        subject_name = table_data[k][4]
        line = cursor.execute("Select subject_id FROM subject WHERE subject_name='%s'" %(subject_name))
        subject_id = cursor.fetchone()
        input = (teacher_id, table_data[k][1], table_data[k][2], table_data[k][3], subject_id)
        cursor.execute(query, input)
        teacher_id += 1
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
    pass
finally:
    if conn is not None:
        pass


#Availability data 

def check_empty(value):
    if value == None:
        return 0
    return value

def slots(time):
    s=int(time[0:2])
    e=int(time[3:])
    ans=[]
    for i in range(s,e):
        a=str(i)
        b=str(i+1)
        ans.append(a+"-"+b)
    return ans

def increment_row():
    increment_row.counter += 1
    return increment_row.counter
increment_row.counter = 0

def get_data():
    lst = []
    file = openpyxl.load_workbook("availability.xlsx")
    f = file.active
    col = 9
    row = f.max_row
    for j in range(1,row+1):
        l = []
        for i in range(1,col + 1):
            data = f.cell(row = j, column= i)
            temp = data.value
            l.append(temp)
        lst.append(l)
    return lst

try:
    conn = None
    conn = psycopg2.connect(
    host="localhost",
    database="postgres",
    user="postgres",
    port="5432",
    password="123")
    cursor = conn.cursor()
    query = "INSERT INTO availability ( teacher_id, subject_name, class_day , class_time) VALUES (%s,%s,%s,%s)"
    query9 = "SELECT teacher_id,subject_name,class_day,class_time FROM availability WHERE teacher_id=%s AND subject_name=%s AND class_day=%s AND class_time=%s;   "
    details = get_data()
    file = openpyxl.load_workbook("availability.xlsx")
    f = file.active
    col = f.max_column
    row = f.max_row
    for k in range(0, len(details)):

        if(check_empty(details[k][2]) != 0):
            x = details[k][2].split(", ")
            a=x[0]
            b=x[1]
            q=slots(a)
            if not b == '':
                q0 = slots(b)
                for i in q0:
                    data = (details[k][0], details[k][1], 'monday', i)
                    cursor.execute(query9, data)
                    lb = cursor.fetchone()
                    if lb==None:
                        #print("inside 1")
                        cursor.execute(query, data)
            for i in q:
                data = (details[k][0], details[k][1], 'monday', i)
                cursor.execute(query9, data)
                lb = cursor.fetchone()
                if lb==None:
                    cursor.execute(query, data)

        if(check_empty(details[k][3]) != 0):
            x1 = details[k][3].split(", ")
            a1 = x1[0]
            b1 = x1[1]
            q1 = slots(a1)
            if not b1 == '':
                q2 = slots(b1)
                for i in q2:
                    data = (details[k][0], details[k][1], 'tuesday', i)
                    cursor.execute(query9, data)
                    lb = cursor.fetchone()
                    if lb==None:
                        cursor.execute(query, data)
                        #print("inside 2")

            for i in q1:
                data = (details[k][0], details[k][1], 'tuesday', i)
                cursor.execute(query9, data)
                lb = cursor.fetchone()
                if lb==None:
                    cursor.execute(query, data)

        if(check_empty(details[k][4]) != 0):
            x2 = details[k][4].split(", ")
            a2 = x2[0]
            b2 = x2[1]
            q3 = slots(a2)

            if not b2 == '':
                q4 = slots(b2)
                for i in q4:
                    data = (details[k][0], details[k][1], 'wednesday', i)
                    cursor.execute(query9, data)
                    lb = cursor.fetchone()
                    if lb==None:
                        cursor.execute(query, data)
                        #print("inside 3")

            for i in q3:
                data = (details[k][0], details[k][1], 'wednesday', i)
                cursor.execute(query9, data)
                lb = cursor.fetchone()
                if lb==None:
                    cursor.execute(query, data)

        if(check_empty(details[k][5]) != 0):
            x3 = details[k][5].split(", ")
            a3 = x3[0]
            b3 = x3[1]
            q5 = slots(a3)
            if not b3 == '':
                q6 = slots(b3)
                for i in q6:
                    data = (details[k][0], details[k][1], 'thursday', i)
                    cursor.execute(query9, data)
                    lb = cursor.fetchone()
                    if lb==None:
                        cursor.execute(query, data)
                        #print("inside 4")
            for i in q5:
                data = (details[k][0], details[k][1], 'thursday', i)
                cursor.execute(query9, data)
                lb = cursor.fetchone()
                if lb==None:
                    cursor.execute(query, data)

        if(check_empty(details[k][6]) != 0):
            x4 = details[k][6].split(", ")
            a4 = x4[0]
            b4 = x4[1]
            q7 = slots(a4)

            if not b4 == '':
                q8 = slots(b4)
                for i in q8:
                    data = (details[k][0], details[k][1], 'friday', i)
                    cursor.execute(query9, data)
                    lb = cursor.fetchone()
                    if lb==None:
                        cursor.execute(query, data)
                        #print("inside 5")
            for i in q7:
                data = (details[k][0], details[k][1], 'friday', i)
                cursor.execute(query9, data)
                lb = cursor.fetchone()
                if lb==None:
                    cursor.execute(query, data)

        if(check_empty(details[k][7]) != 0):
            x5 = details[k][7].split(", ")
            a5 = x5[0]
            b5 = x5[1]
            q9 = slots(a5)

            if not b5 == '':
                q10 = slots(b5)
                for i in q10:
                    data = (details[k][0], details[k][1], 'saturday', i)
                    cursor.execute(query9, data)
                    lb = cursor.fetchone()
                    if lb==None:
                        cursor.execute(query, data)
                        #print("inside 6")
            for i in q9:
                data = (details[k][0], details[k][1], 'saturday', i)
                cursor.execute(query9, data)
                lb = cursor.fetchone()
                if lb==None:
                    cursor.execute(query, data)

        if(check_empty(details[k][8]) != 0):
            x6 = details[k][8].split(", ")
            a6 = x6[0]
            b6 = x6[1]
            q11 = slots(a6)

            if not b6 == '':
                q12= slots(b6)
                for i in q12:

                    data = (details[k][0], details[k][1], 'sunday', i)
                    cursor.execute(query9, data)
                    lb = cursor.fetchone()
                    if lb==None:
                        cursor.execute(query, data)
                       
            for i in q11:
                data = (details[k][0], details[k][1], 'sunday', i)
                cursor.execute(query9, data)
                lb = cursor.fetchone()
                if lb==None:
                    cursor.execute(query, data)
    
    conn.commit()

except(Exception, psycopg2.DatabaseError) as error:
        print(error)
finally:
    if conn is not None:
        pass

conn = psycopg2.connect(
    host="localhost",
    database="postgres",
    user="postgres",
    port="5432",
    password="123")
cursor=conn.cursor()
cursor.execute('SELECT DISTINCT * FROM availability ORDER BY teacher_id, class_time;')
avail=cursor.fetchall()
print("Available Slots :")
for row in avail:
    print(row)
cursor.execute('Select * from booked')
print("Booked Slots :")
avail1=cursor.fetchall()
for row in avail1:
    print(row)

estudent_id = int(input("Enter student's id : "))
eteacher_id = int(input("Enter teachers's id : "))
esubject_name = input("Enter subject name : ")
eclass_day = input("Enter day : ")
eclass_time = input("Enter class timing: ")

def booking_slot(estudent_id, eteacher_id, esubject_name, eclass_day, eclass_time):
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        port="5432",
        password="123")
    cursor = conn.cursor()

    query = """INSERT INTO booked ( student_id, teacher_id, subject_name, class_day, class_time) VALUES (%s,%s,%s,%s,%s)"""

    #CHECK subject_name
    cursor.execute(
        "SELECT subject_name FROM availability WHERE subject_name = %s ",
        (esubject_name,)
    )
    row_count = cursor.rowcount
    if row_count == 0:
        print("It Does Not Exist!")
        return
    #CHECK student_id
    cursor.execute(
        "SELECT student_id FROM student WHERE student_id = %s ",
        (estudent_id,)
    )
    row_count = cursor.rowcount
    if row_count == 0:
        print("It Does Not Exist!")
        return
    ##CHECK class_day
    cursor.execute(
        "SELECT class_day FROM availability WHERE class_day = %s ",
        (eclass_day,)
    )
    row_count = cursor.rowcount
    if row_count == 0:
        print("It Does Not Exist!")
        return
    #CHECK class_time
    cursor.execute(
        "SELECT class_time FROM availability WHERE class_time= %s ",
        (eclass_time,)
    )
    row_count = cursor.rowcount
    if row_count == 0:
        print("It Does Not Exist!")
        return
    #CHECK teacher_id 
        "SELECT teacher_id FROM availability WHERE teacher_id = %s ",
        (eteacher_id,)
    )
    row_count = cursor.rowcount
    if row_count == 0:
        print("It Does Not Exist!")
        return
    cursor.execute(
        "DELETE FROM availability WHERE teacher_id=%s AND subject_name=%s AND class_day=%s AND class_time=%s",
        (eteacher_id,esubject_name,eclass_day,eclass_time,)
    )
    insert = (estudent_id,eteacher_id,esubject_name,eclass_day,eclass_time)
    cursor.execute(query,insert)
    print("Slot booked!")
    conn.commit()
    conn.close()

booking_slot(estudent_id, eteacher_id, esubject_name, eclass_day, eclass_time)